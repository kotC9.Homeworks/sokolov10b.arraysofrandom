﻿namespace sokolov10b.arraysofrandom
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxSourceArrays = new System.Windows.Forms.GroupBox();
            this.labelElement0 = new System.Windows.Forms.Label();
            this.element0 = new System.Windows.Forms.TextBox();
            this.element1 = new System.Windows.Forms.TextBox();
            this.labelElement1 = new System.Windows.Forms.Label();
            this.element2 = new System.Windows.Forms.TextBox();
            this.labelElement2 = new System.Windows.Forms.Label();
            this.element3 = new System.Windows.Forms.TextBox();
            this.labelElement3 = new System.Windows.Forms.Label();
            this.element4 = new System.Windows.Forms.TextBox();
            this.labelElement4 = new System.Windows.Forms.Label();
            this.element5 = new System.Windows.Forms.TextBox();
            this.labelElement5 = new System.Windows.Forms.Label();
            this.element6 = new System.Windows.Forms.TextBox();
            this.labelElement6 = new System.Windows.Forms.Label();
            this.element7 = new System.Windows.Forms.TextBox();
            this.labelElement7 = new System.Windows.Forms.Label();
            this.element8 = new System.Windows.Forms.TextBox();
            this.labelElement8 = new System.Windows.Forms.Label();
            this.element9 = new System.Windows.Forms.TextBox();
            this.labelElement9 = new System.Windows.Forms.Label();
            this.groupBoxRange = new System.Windows.Forms.GroupBox();
            this.labelRangeFrom = new System.Windows.Forms.Label();
            this.labelabelRangeTo = new System.Windows.Forms.Label();
            this.rangeFrom = new System.Windows.Forms.TextBox();
            this.rangeTo = new System.Windows.Forms.TextBox();
            this.buttonFillArray = new System.Windows.Forms.Button();
            this.labelStats = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelMinimum = new System.Windows.Forms.Label();
            this.labelMinimumElementIndex = new System.Windows.Forms.Label();
            this.minimumElementIndex = new System.Windows.Forms.TextBox();
            this.labelMinimumElementValue = new System.Windows.Forms.Label();
            this.minimumElementValue = new System.Windows.Forms.TextBox();
            this.maximumElementValue = new System.Windows.Forms.TextBox();
            this.labelMaximumElementValue = new System.Windows.Forms.Label();
            this.maximumElementIndex = new System.Windows.Forms.TextBox();
            this.labelMaximumElementIndex = new System.Windows.Forms.Label();
            this.labelMaximum = new System.Windows.Forms.Label();
            this.buttonCalculateStatistics = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelSumElements = new System.Windows.Forms.Label();
            this.textBoxSumElements = new System.Windows.Forms.TextBox();
            this.textBoxAverage = new System.Windows.Forms.TextBox();
            this.labelAverage = new System.Windows.Forms.Label();
            this.textBoxSumPositive = new System.Windows.Forms.TextBox();
            this.labelSumPositive = new System.Windows.Forms.Label();
            this.textBoxSumNegative = new System.Windows.Forms.TextBox();
            this.labelSumNegative = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelCountPositiveEven = new System.Windows.Forms.Label();
            this.textBoxCountPositiveEven = new System.Windows.Forms.TextBox();
            this.textBoxCountPositiveOdd = new System.Windows.Forms.TextBox();
            this.labelCountPositiveOdd = new System.Windows.Forms.Label();
            this.textBoxCountNegativeEven = new System.Windows.Forms.TextBox();
            this.labelCountNegativeEven = new System.Windows.Forms.Label();
            this.textBoxCountNegativeOdd = new System.Windows.Forms.TextBox();
            this.labelCountNegativeOdd = new System.Windows.Forms.Label();
            this.labelMultiplication = new System.Windows.Forms.Label();
            this.textBoxMultiplication = new System.Windows.Forms.TextBox();
            this.buttonReverseArray = new System.Windows.Forms.Button();
            this.buttonSortArray = new System.Windows.Forms.Button();
            this.groupBoxResultArray = new System.Windows.Forms.GroupBox();
            this.textBoxResultElement9 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxResultElement8 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxResultElement7 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxResultElement6 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxResultElement5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxResultElement4 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxResultElement3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxResultElement2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxResultElement1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxResultElement0 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBoxSourceArrays.SuspendLayout();
            this.groupBoxRange.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBoxResultArray.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxSourceArrays
            // 
            this.groupBoxSourceArrays.Controls.Add(this.element9);
            this.groupBoxSourceArrays.Controls.Add(this.labelElement9);
            this.groupBoxSourceArrays.Controls.Add(this.element8);
            this.groupBoxSourceArrays.Controls.Add(this.labelElement8);
            this.groupBoxSourceArrays.Controls.Add(this.element7);
            this.groupBoxSourceArrays.Controls.Add(this.labelElement7);
            this.groupBoxSourceArrays.Controls.Add(this.element6);
            this.groupBoxSourceArrays.Controls.Add(this.labelElement6);
            this.groupBoxSourceArrays.Controls.Add(this.element5);
            this.groupBoxSourceArrays.Controls.Add(this.labelElement5);
            this.groupBoxSourceArrays.Controls.Add(this.element4);
            this.groupBoxSourceArrays.Controls.Add(this.labelElement4);
            this.groupBoxSourceArrays.Controls.Add(this.element3);
            this.groupBoxSourceArrays.Controls.Add(this.labelElement3);
            this.groupBoxSourceArrays.Controls.Add(this.element2);
            this.groupBoxSourceArrays.Controls.Add(this.labelElement2);
            this.groupBoxSourceArrays.Controls.Add(this.element1);
            this.groupBoxSourceArrays.Controls.Add(this.labelElement1);
            this.groupBoxSourceArrays.Controls.Add(this.element0);
            this.groupBoxSourceArrays.Controls.Add(this.labelElement0);
            this.groupBoxSourceArrays.Location = new System.Drawing.Point(12, 83);
            this.groupBoxSourceArrays.Name = "groupBoxSourceArrays";
            this.groupBoxSourceArrays.Size = new System.Drawing.Size(639, 110);
            this.groupBoxSourceArrays.TabIndex = 0;
            this.groupBoxSourceArrays.TabStop = false;
            this.groupBoxSourceArrays.Text = "Исходный массив";
            // 
            // labelElement0
            // 
            this.labelElement0.Location = new System.Drawing.Point(30, 28);
            this.labelElement0.Name = "labelElement0";
            this.labelElement0.Size = new System.Drawing.Size(52, 23);
            this.labelElement0.TabIndex = 1;
            this.labelElement0.Text = "1";
            this.labelElement0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // element0
            // 
            this.element0.Location = new System.Drawing.Point(30, 54);
            this.element0.Name = "element0";
            this.element0.ReadOnly = true;
            this.element0.Size = new System.Drawing.Size(52, 23);
            this.element0.TabIndex = 2;
            // 
            // element1
            // 
            this.element1.Location = new System.Drawing.Point(88, 54);
            this.element1.Name = "element1";
            this.element1.ReadOnly = true;
            this.element1.Size = new System.Drawing.Size(52, 23);
            this.element1.TabIndex = 4;
            // 
            // labelElement1
            // 
            this.labelElement1.Location = new System.Drawing.Point(88, 28);
            this.labelElement1.Name = "labelElement1";
            this.labelElement1.Size = new System.Drawing.Size(52, 23);
            this.labelElement1.TabIndex = 3;
            this.labelElement1.Text = "2";
            this.labelElement1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // element2
            // 
            this.element2.Location = new System.Drawing.Point(146, 54);
            this.element2.Name = "element2";
            this.element2.ReadOnly = true;
            this.element2.Size = new System.Drawing.Size(52, 23);
            this.element2.TabIndex = 6;
            // 
            // labelElement2
            // 
            this.labelElement2.Location = new System.Drawing.Point(146, 28);
            this.labelElement2.Name = "labelElement2";
            this.labelElement2.Size = new System.Drawing.Size(52, 23);
            this.labelElement2.TabIndex = 5;
            this.labelElement2.Text = "3";
            this.labelElement2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // element3
            // 
            this.element3.Location = new System.Drawing.Point(204, 54);
            this.element3.Name = "element3";
            this.element3.ReadOnly = true;
            this.element3.Size = new System.Drawing.Size(52, 23);
            this.element3.TabIndex = 8;
            // 
            // labelElement3
            // 
            this.labelElement3.Location = new System.Drawing.Point(204, 28);
            this.labelElement3.Name = "labelElement3";
            this.labelElement3.Size = new System.Drawing.Size(52, 23);
            this.labelElement3.TabIndex = 7;
            this.labelElement3.Text = "4";
            this.labelElement3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // element4
            // 
            this.element4.Location = new System.Drawing.Point(262, 54);
            this.element4.Name = "element4";
            this.element4.ReadOnly = true;
            this.element4.Size = new System.Drawing.Size(52, 23);
            this.element4.TabIndex = 10;
            // 
            // labelElement4
            // 
            this.labelElement4.Location = new System.Drawing.Point(262, 28);
            this.labelElement4.Name = "labelElement4";
            this.labelElement4.Size = new System.Drawing.Size(52, 23);
            this.labelElement4.TabIndex = 9;
            this.labelElement4.Text = "5";
            this.labelElement4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // element5
            // 
            this.element5.Location = new System.Drawing.Point(320, 54);
            this.element5.Name = "element5";
            this.element5.ReadOnly = true;
            this.element5.Size = new System.Drawing.Size(52, 23);
            this.element5.TabIndex = 12;
            // 
            // labelElement5
            // 
            this.labelElement5.Location = new System.Drawing.Point(320, 28);
            this.labelElement5.Name = "labelElement5";
            this.labelElement5.Size = new System.Drawing.Size(52, 23);
            this.labelElement5.TabIndex = 11;
            this.labelElement5.Text = "6";
            this.labelElement5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // element6
            // 
            this.element6.Location = new System.Drawing.Point(378, 54);
            this.element6.Name = "element6";
            this.element6.ReadOnly = true;
            this.element6.Size = new System.Drawing.Size(52, 23);
            this.element6.TabIndex = 14;
            // 
            // labelElement6
            // 
            this.labelElement6.Location = new System.Drawing.Point(378, 28);
            this.labelElement6.Name = "labelElement6";
            this.labelElement6.Size = new System.Drawing.Size(52, 23);
            this.labelElement6.TabIndex = 13;
            this.labelElement6.Text = "7";
            this.labelElement6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // element7
            // 
            this.element7.Location = new System.Drawing.Point(436, 54);
            this.element7.Name = "element7";
            this.element7.ReadOnly = true;
            this.element7.Size = new System.Drawing.Size(52, 23);
            this.element7.TabIndex = 16;
            // 
            // labelElement7
            // 
            this.labelElement7.Location = new System.Drawing.Point(436, 28);
            this.labelElement7.Name = "labelElement7";
            this.labelElement7.Size = new System.Drawing.Size(52, 23);
            this.labelElement7.TabIndex = 15;
            this.labelElement7.Text = "8";
            this.labelElement7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // element8
            // 
            this.element8.Location = new System.Drawing.Point(494, 54);
            this.element8.Name = "element8";
            this.element8.ReadOnly = true;
            this.element8.Size = new System.Drawing.Size(52, 23);
            this.element8.TabIndex = 18;
            // 
            // labelElement8
            // 
            this.labelElement8.Location = new System.Drawing.Point(494, 28);
            this.labelElement8.Name = "labelElement8";
            this.labelElement8.Size = new System.Drawing.Size(52, 23);
            this.labelElement8.TabIndex = 17;
            this.labelElement8.Text = "9";
            this.labelElement8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // element9
            // 
            this.element9.Location = new System.Drawing.Point(552, 54);
            this.element9.Name = "element9";
            this.element9.ReadOnly = true;
            this.element9.Size = new System.Drawing.Size(52, 23);
            this.element9.TabIndex = 20;
            // 
            // labelElement9
            // 
            this.labelElement9.Location = new System.Drawing.Point(552, 28);
            this.labelElement9.Name = "labelElement9";
            this.labelElement9.Size = new System.Drawing.Size(52, 23);
            this.labelElement9.TabIndex = 19;
            this.labelElement9.Text = "10";
            this.labelElement9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxRange
            // 
            this.groupBoxRange.Controls.Add(this.rangeTo);
            this.groupBoxRange.Controls.Add(this.rangeFrom);
            this.groupBoxRange.Controls.Add(this.labelabelRangeTo);
            this.groupBoxRange.Controls.Add(this.labelRangeFrom);
            this.groupBoxRange.Location = new System.Drawing.Point(12, 12);
            this.groupBoxRange.Name = "groupBoxRange";
            this.groupBoxRange.Size = new System.Drawing.Size(200, 65);
            this.groupBoxRange.TabIndex = 1;
            this.groupBoxRange.TabStop = false;
            this.groupBoxRange.Text = "Границы диапазона";
            // 
            // labelRangeFrom
            // 
            this.labelRangeFrom.Location = new System.Drawing.Point(19, 22);
            this.labelRangeFrom.Name = "labelRangeFrom";
            this.labelRangeFrom.Size = new System.Drawing.Size(23, 23);
            this.labelRangeFrom.TabIndex = 0;
            this.labelRangeFrom.Text = "с";
            // 
            // labelabelRangeTo
            // 
            this.labelabelRangeTo.Location = new System.Drawing.Point(93, 22);
            this.labelabelRangeTo.Name = "labelabelRangeTo";
            this.labelabelRangeTo.Size = new System.Drawing.Size(23, 23);
            this.labelabelRangeTo.TabIndex = 1;
            this.labelabelRangeTo.Text = "по";
            // 
            // rangeFrom
            // 
            this.rangeFrom.Location = new System.Drawing.Point(35, 22);
            this.rangeFrom.Name = "rangeFrom";
            this.rangeFrom.Size = new System.Drawing.Size(52, 23);
            this.rangeFrom.TabIndex = 2;
            // 
            // rangeTo
            // 
            this.rangeTo.Location = new System.Drawing.Point(122, 22);
            this.rangeTo.Name = "rangeTo";
            this.rangeTo.Size = new System.Drawing.Size(52, 23);
            this.rangeTo.TabIndex = 3;
            // 
            // buttonFillArray
            // 
            this.buttonFillArray.Location = new System.Drawing.Point(362, 17);
            this.buttonFillArray.Name = "buttonFillArray";
            this.buttonFillArray.Size = new System.Drawing.Size(118, 54);
            this.buttonFillArray.TabIndex = 2;
            this.buttonFillArray.Text = "Заполнить массив целыми числами";
            this.buttonFillArray.UseVisualStyleBackColor = true;
            this.buttonFillArray.Click += new System.EventHandler(this.buttonFillArray_Click);
            // 
            // labelStats
            // 
            this.labelStats.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.labelStats.ForeColor = System.Drawing.Color.Blue;
            this.labelStats.Location = new System.Drawing.Point(12, 211);
            this.labelStats.Name = "labelStats";
            this.labelStats.Size = new System.Drawing.Size(170, 51);
            this.labelStats.TabIndex = 3;
            this.labelStats.Text = "Статистика";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.maximumElementValue);
            this.groupBox1.Controls.Add(this.labelMaximumElementValue);
            this.groupBox1.Controls.Add(this.maximumElementIndex);
            this.groupBox1.Controls.Add(this.labelMaximumElementIndex);
            this.groupBox1.Controls.Add(this.labelMaximum);
            this.groupBox1.Controls.Add(this.minimumElementValue);
            this.groupBox1.Controls.Add(this.labelMinimumElementValue);
            this.groupBox1.Controls.Add(this.minimumElementIndex);
            this.groupBox1.Controls.Add(this.labelMinimumElementIndex);
            this.groupBox1.Controls.Add(this.labelMinimum);
            this.groupBox1.Location = new System.Drawing.Point(12, 265);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(385, 109);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "1";
            // 
            // labelMinimum
            // 
            this.labelMinimum.Location = new System.Drawing.Point(9, 19);
            this.labelMinimum.Name = "labelMinimum";
            this.labelMinimum.Size = new System.Drawing.Size(75, 23);
            this.labelMinimum.TabIndex = 0;
            this.labelMinimum.Text = "Минимум:";
            // 
            // labelMinimumElementIndex
            // 
            this.labelMinimumElementIndex.Location = new System.Drawing.Point(74, 19);
            this.labelMinimumElementIndex.Name = "labelMinimumElementIndex";
            this.labelMinimumElementIndex.Size = new System.Drawing.Size(100, 23);
            this.labelMinimumElementIndex.TabIndex = 1;
            this.labelMinimumElementIndex.Text = "номер элемента";
            // 
            // minimumElementIndex
            // 
            this.minimumElementIndex.Location = new System.Drawing.Point(178, 16);
            this.minimumElementIndex.Name = "minimumElementIndex";
            this.minimumElementIndex.Size = new System.Drawing.Size(52, 23);
            this.minimumElementIndex.TabIndex = 2;
            // 
            // labelMinimumElementValue
            // 
            this.labelMinimumElementValue.Location = new System.Drawing.Point(246, 19);
            this.labelMinimumElementValue.Name = "labelMinimumElementValue";
            this.labelMinimumElementValue.Size = new System.Drawing.Size(63, 23);
            this.labelMinimumElementValue.TabIndex = 3;
            this.labelMinimumElementValue.Text = "значение";
            // 
            // minimumElementValue
            // 
            this.minimumElementValue.Location = new System.Drawing.Point(315, 16);
            this.minimumElementValue.Name = "minimumElementValue";
            this.minimumElementValue.Size = new System.Drawing.Size(52, 23);
            this.minimumElementValue.TabIndex = 4;
            // 
            // maximumElementValue
            // 
            this.maximumElementValue.Location = new System.Drawing.Point(315, 56);
            this.maximumElementValue.Name = "maximumElementValue";
            this.maximumElementValue.Size = new System.Drawing.Size(52, 23);
            this.maximumElementValue.TabIndex = 9;
            // 
            // labelMaximumElementValue
            // 
            this.labelMaximumElementValue.Location = new System.Drawing.Point(246, 59);
            this.labelMaximumElementValue.Name = "labelMaximumElementValue";
            this.labelMaximumElementValue.Size = new System.Drawing.Size(63, 23);
            this.labelMaximumElementValue.TabIndex = 8;
            this.labelMaximumElementValue.Text = "значение";
            // 
            // maximumElementIndex
            // 
            this.maximumElementIndex.Location = new System.Drawing.Point(178, 56);
            this.maximumElementIndex.Name = "maximumElementIndex";
            this.maximumElementIndex.Size = new System.Drawing.Size(52, 23);
            this.maximumElementIndex.TabIndex = 7;
            // 
            // labelMaximumElementIndex
            // 
            this.labelMaximumElementIndex.Location = new System.Drawing.Point(74, 59);
            this.labelMaximumElementIndex.Name = "labelMaximumElementIndex";
            this.labelMaximumElementIndex.Size = new System.Drawing.Size(100, 23);
            this.labelMaximumElementIndex.TabIndex = 6;
            this.labelMaximumElementIndex.Text = "номер элемента";
            // 
            // labelMaximum
            // 
            this.labelMaximum.Location = new System.Drawing.Point(9, 59);
            this.labelMaximum.Name = "labelMaximum";
            this.labelMaximum.Size = new System.Drawing.Size(75, 23);
            this.labelMaximum.TabIndex = 5;
            this.labelMaximum.Text = "Максимум:";
            // 
            // buttonCalculateStatistics
            // 
            this.buttonCalculateStatistics.Location = new System.Drawing.Point(422, 281);
            this.buttonCalculateStatistics.Name = "buttonCalculateStatistics";
            this.buttonCalculateStatistics.Size = new System.Drawing.Size(156, 38);
            this.buttonCalculateStatistics.TabIndex = 5;
            this.buttonCalculateStatistics.Text = "Статистика";
            this.buttonCalculateStatistics.UseVisualStyleBackColor = true;
            this.buttonCalculateStatistics.Click += new System.EventHandler(this.buttonCalculateStatistics_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxSumNegative);
            this.groupBox2.Controls.Add(this.labelSumNegative);
            this.groupBox2.Controls.Add(this.textBoxSumPositive);
            this.groupBox2.Controls.Add(this.labelSumPositive);
            this.groupBox2.Controls.Add(this.textBoxAverage);
            this.groupBox2.Controls.Add(this.labelAverage);
            this.groupBox2.Controls.Add(this.textBoxSumElements);
            this.groupBox2.Controls.Add(this.labelSumElements);
            this.groupBox2.Location = new System.Drawing.Point(12, 380);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(385, 163);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "2";
            // 
            // labelSumElements
            // 
            this.labelSumElements.Location = new System.Drawing.Point(14, 28);
            this.labelSumElements.Name = "labelSumElements";
            this.labelSumElements.Size = new System.Drawing.Size(156, 23);
            this.labelSumElements.TabIndex = 0;
            this.labelSumElements.Text = "Сумма всех элементов";
            // 
            // textBoxSumElements
            // 
            this.textBoxSumElements.Location = new System.Drawing.Point(246, 28);
            this.textBoxSumElements.Name = "textBoxSumElements";
            this.textBoxSumElements.Size = new System.Drawing.Size(126, 23);
            this.textBoxSumElements.TabIndex = 1;
            // 
            // textBoxAverage
            // 
            this.textBoxAverage.Location = new System.Drawing.Point(246, 61);
            this.textBoxAverage.Name = "textBoxAverage";
            this.textBoxAverage.Size = new System.Drawing.Size(126, 23);
            this.textBoxAverage.TabIndex = 3;
            // 
            // labelAverage
            // 
            this.labelAverage.Location = new System.Drawing.Point(14, 61);
            this.labelAverage.Name = "labelAverage";
            this.labelAverage.Size = new System.Drawing.Size(156, 23);
            this.labelAverage.TabIndex = 2;
            this.labelAverage.Text = "Среднее значение";
            // 
            // textBoxSumPositive
            // 
            this.textBoxSumPositive.Location = new System.Drawing.Point(246, 95);
            this.textBoxSumPositive.Name = "textBoxSumPositive";
            this.textBoxSumPositive.Size = new System.Drawing.Size(126, 23);
            this.textBoxSumPositive.TabIndex = 5;
            // 
            // labelSumPositive
            // 
            this.labelSumPositive.Location = new System.Drawing.Point(14, 95);
            this.labelSumPositive.Name = "labelSumPositive";
            this.labelSumPositive.Size = new System.Drawing.Size(156, 23);
            this.labelSumPositive.TabIndex = 4;
            this.labelSumPositive.Text = "Сумма положительных элементов";
            // 
            // textBoxSumNegative
            // 
            this.textBoxSumNegative.Location = new System.Drawing.Point(246, 128);
            this.textBoxSumNegative.Name = "textBoxSumNegative";
            this.textBoxSumNegative.Size = new System.Drawing.Size(126, 23);
            this.textBoxSumNegative.TabIndex = 7;
            // 
            // labelSumNegative
            // 
            this.labelSumNegative.Location = new System.Drawing.Point(14, 128);
            this.labelSumNegative.Name = "labelSumNegative";
            this.labelSumNegative.Size = new System.Drawing.Size(156, 23);
            this.labelSumNegative.TabIndex = 6;
            this.labelSumNegative.Text = "Сумма отрицательных элементов";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxCountNegativeOdd);
            this.groupBox3.Controls.Add(this.textBoxCountNegativeEven);
            this.groupBox3.Controls.Add(this.labelCountNegativeOdd);
            this.groupBox3.Controls.Add(this.labelCountNegativeEven);
            this.groupBox3.Controls.Add(this.textBoxCountPositiveOdd);
            this.groupBox3.Controls.Add(this.labelCountPositiveOdd);
            this.groupBox3.Controls.Add(this.textBoxCountPositiveEven);
            this.groupBox3.Controls.Add(this.labelCountPositiveEven);
            this.groupBox3.Location = new System.Drawing.Point(422, 380);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(314, 163);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "3";
            // 
            // labelCountPositiveEven
            // 
            this.labelCountPositiveEven.Location = new System.Drawing.Point(10, 28);
            this.labelCountPositiveEven.Name = "labelCountPositiveEven";
            this.labelCountPositiveEven.Size = new System.Drawing.Size(228, 23);
            this.labelCountPositiveEven.TabIndex = 0;
            this.labelCountPositiveEven.Text = "Количество положительных четных";
            // 
            // textBoxCountPositiveEven
            // 
            this.textBoxCountPositiveEven.Location = new System.Drawing.Point(256, 25);
            this.textBoxCountPositiveEven.Name = "textBoxCountPositiveEven";
            this.textBoxCountPositiveEven.Size = new System.Drawing.Size(52, 23);
            this.textBoxCountPositiveEven.TabIndex = 1;
            // 
            // textBoxCountPositiveOdd
            // 
            this.textBoxCountPositiveOdd.Location = new System.Drawing.Point(256, 61);
            this.textBoxCountPositiveOdd.Name = "textBoxCountPositiveOdd";
            this.textBoxCountPositiveOdd.Size = new System.Drawing.Size(52, 23);
            this.textBoxCountPositiveOdd.TabIndex = 3;
            // 
            // labelCountPositiveOdd
            // 
            this.labelCountPositiveOdd.Location = new System.Drawing.Point(10, 64);
            this.labelCountPositiveOdd.Name = "labelCountPositiveOdd";
            this.labelCountPositiveOdd.Size = new System.Drawing.Size(228, 23);
            this.labelCountPositiveOdd.TabIndex = 2;
            this.labelCountPositiveOdd.Text = "Количество положительных нечетных";
            // 
            // textBoxCountNegativeEven
            // 
            this.textBoxCountNegativeEven.Location = new System.Drawing.Point(256, 92);
            this.textBoxCountNegativeEven.Name = "textBoxCountNegativeEven";
            this.textBoxCountNegativeEven.Size = new System.Drawing.Size(52, 23);
            this.textBoxCountNegativeEven.TabIndex = 5;
            // 
            // labelCountNegativeEven
            // 
            this.labelCountNegativeEven.Location = new System.Drawing.Point(10, 95);
            this.labelCountNegativeEven.Name = "labelCountNegativeEven";
            this.labelCountNegativeEven.Size = new System.Drawing.Size(228, 23);
            this.labelCountNegativeEven.TabIndex = 4;
            this.labelCountNegativeEven.Text = "Количество отрицательных четных";
            // 
            // textBoxCountNegativeOdd
            // 
            this.textBoxCountNegativeOdd.Location = new System.Drawing.Point(256, 125);
            this.textBoxCountNegativeOdd.Name = "textBoxCountNegativeOdd";
            this.textBoxCountNegativeOdd.Size = new System.Drawing.Size(52, 23);
            this.textBoxCountNegativeOdd.TabIndex = 9;
            // 
            // labelCountNegativeOdd
            // 
            this.labelCountNegativeOdd.Location = new System.Drawing.Point(10, 128);
            this.labelCountNegativeOdd.Name = "labelCountNegativeOdd";
            this.labelCountNegativeOdd.Size = new System.Drawing.Size(228, 23);
            this.labelCountNegativeOdd.TabIndex = 8;
            this.labelCountNegativeOdd.Text = "Количество отрицательных четных";
            // 
            // labelMultiplication
            // 
            this.labelMultiplication.Location = new System.Drawing.Point(12, 568);
            this.labelMultiplication.Name = "labelMultiplication";
            this.labelMultiplication.Size = new System.Drawing.Size(211, 23);
            this.labelMultiplication.TabIndex = 8;
            this.labelMultiplication.Text = "Произведение элементов массива";
            // 
            // textBoxMultiplication
            // 
            this.textBoxMultiplication.Location = new System.Drawing.Point(229, 565);
            this.textBoxMultiplication.Name = "textBoxMultiplication";
            this.textBoxMultiplication.Size = new System.Drawing.Size(213, 23);
            this.textBoxMultiplication.TabIndex = 9;
            // 
            // buttonReverseArray
            // 
            this.buttonReverseArray.Location = new System.Drawing.Point(45, 653);
            this.buttonReverseArray.Name = "buttonReverseArray";
            this.buttonReverseArray.Size = new System.Drawing.Size(137, 39);
            this.buttonReverseArray.TabIndex = 10;
            this.buttonReverseArray.Text = "Реверс массива";
            this.buttonReverseArray.UseVisualStyleBackColor = true;
            this.buttonReverseArray.Click += new System.EventHandler(this.buttonReverseArray_Click);
            // 
            // buttonSortArray
            // 
            this.buttonSortArray.Location = new System.Drawing.Point(216, 653);
            this.buttonSortArray.Name = "buttonSortArray";
            this.buttonSortArray.Size = new System.Drawing.Size(226, 39);
            this.buttonSortArray.TabIndex = 11;
            this.buttonSortArray.Text = "Отсортировать по возрастанию";
            this.buttonSortArray.UseVisualStyleBackColor = true;
            this.buttonSortArray.Click += new System.EventHandler(this.buttonSortArray_Click);
            // 
            // groupBoxResultArray
            // 
            this.groupBoxResultArray.Controls.Add(this.textBoxResultElement9);
            this.groupBoxResultArray.Controls.Add(this.label1);
            this.groupBoxResultArray.Controls.Add(this.textBoxResultElement8);
            this.groupBoxResultArray.Controls.Add(this.label2);
            this.groupBoxResultArray.Controls.Add(this.textBoxResultElement7);
            this.groupBoxResultArray.Controls.Add(this.label3);
            this.groupBoxResultArray.Controls.Add(this.textBoxResultElement6);
            this.groupBoxResultArray.Controls.Add(this.label4);
            this.groupBoxResultArray.Controls.Add(this.textBoxResultElement5);
            this.groupBoxResultArray.Controls.Add(this.label5);
            this.groupBoxResultArray.Controls.Add(this.textBoxResultElement4);
            this.groupBoxResultArray.Controls.Add(this.label6);
            this.groupBoxResultArray.Controls.Add(this.textBoxResultElement3);
            this.groupBoxResultArray.Controls.Add(this.label7);
            this.groupBoxResultArray.Controls.Add(this.textBoxResultElement2);
            this.groupBoxResultArray.Controls.Add(this.label8);
            this.groupBoxResultArray.Controls.Add(this.textBoxResultElement1);
            this.groupBoxResultArray.Controls.Add(this.label9);
            this.groupBoxResultArray.Controls.Add(this.textBoxResultElement0);
            this.groupBoxResultArray.Controls.Add(this.label10);
            this.groupBoxResultArray.Location = new System.Drawing.Point(47, 698);
            this.groupBoxResultArray.Name = "groupBoxResultArray";
            this.groupBoxResultArray.Size = new System.Drawing.Size(639, 110);
            this.groupBoxResultArray.TabIndex = 12;
            this.groupBoxResultArray.TabStop = false;
            this.groupBoxResultArray.Text = "Массив-результат";
            // 
            // textBoxResultElement9
            // 
            this.textBoxResultElement9.Location = new System.Drawing.Point(552, 54);
            this.textBoxResultElement9.Name = "textBoxResultElement9";
            this.textBoxResultElement9.ReadOnly = true;
            this.textBoxResultElement9.Size = new System.Drawing.Size(52, 23);
            this.textBoxResultElement9.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(552, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 23);
            this.label1.TabIndex = 19;
            this.label1.Text = "10";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultElement8
            // 
            this.textBoxResultElement8.Location = new System.Drawing.Point(494, 54);
            this.textBoxResultElement8.Name = "textBoxResultElement8";
            this.textBoxResultElement8.ReadOnly = true;
            this.textBoxResultElement8.Size = new System.Drawing.Size(52, 23);
            this.textBoxResultElement8.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(494, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 23);
            this.label2.TabIndex = 17;
            this.label2.Text = "9";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultElement7
            // 
            this.textBoxResultElement7.Location = new System.Drawing.Point(436, 54);
            this.textBoxResultElement7.Name = "textBoxResultElement7";
            this.textBoxResultElement7.ReadOnly = true;
            this.textBoxResultElement7.Size = new System.Drawing.Size(52, 23);
            this.textBoxResultElement7.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(436, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 23);
            this.label3.TabIndex = 15;
            this.label3.Text = "8";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultElement6
            // 
            this.textBoxResultElement6.Location = new System.Drawing.Point(378, 54);
            this.textBoxResultElement6.Name = "textBoxResultElement6";
            this.textBoxResultElement6.ReadOnly = true;
            this.textBoxResultElement6.Size = new System.Drawing.Size(52, 23);
            this.textBoxResultElement6.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(378, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 23);
            this.label4.TabIndex = 13;
            this.label4.Text = "7";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultElement5
            // 
            this.textBoxResultElement5.Location = new System.Drawing.Point(320, 54);
            this.textBoxResultElement5.Name = "textBoxResultElement5";
            this.textBoxResultElement5.ReadOnly = true;
            this.textBoxResultElement5.Size = new System.Drawing.Size(52, 23);
            this.textBoxResultElement5.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(320, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 23);
            this.label5.TabIndex = 11;
            this.label5.Text = "6";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultElement4
            // 
            this.textBoxResultElement4.Location = new System.Drawing.Point(262, 54);
            this.textBoxResultElement4.Name = "textBoxResultElement4";
            this.textBoxResultElement4.ReadOnly = true;
            this.textBoxResultElement4.Size = new System.Drawing.Size(52, 23);
            this.textBoxResultElement4.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(262, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 23);
            this.label6.TabIndex = 9;
            this.label6.Text = "5";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultElement3
            // 
            this.textBoxResultElement3.Location = new System.Drawing.Point(204, 54);
            this.textBoxResultElement3.Name = "textBoxResultElement3";
            this.textBoxResultElement3.ReadOnly = true;
            this.textBoxResultElement3.Size = new System.Drawing.Size(52, 23);
            this.textBoxResultElement3.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(204, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 23);
            this.label7.TabIndex = 7;
            this.label7.Text = "4";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultElement2
            // 
            this.textBoxResultElement2.Location = new System.Drawing.Point(146, 54);
            this.textBoxResultElement2.Name = "textBoxResultElement2";
            this.textBoxResultElement2.ReadOnly = true;
            this.textBoxResultElement2.Size = new System.Drawing.Size(52, 23);
            this.textBoxResultElement2.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(146, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 23);
            this.label8.TabIndex = 5;
            this.label8.Text = "3";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultElement1
            // 
            this.textBoxResultElement1.Location = new System.Drawing.Point(88, 54);
            this.textBoxResultElement1.Name = "textBoxResultElement1";
            this.textBoxResultElement1.ReadOnly = true;
            this.textBoxResultElement1.Size = new System.Drawing.Size(52, 23);
            this.textBoxResultElement1.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(88, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 23);
            this.label9.TabIndex = 3;
            this.label9.Text = "2";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultElement0
            // 
            this.textBoxResultElement0.Location = new System.Drawing.Point(30, 54);
            this.textBoxResultElement0.Name = "textBoxResultElement0";
            this.textBoxResultElement0.ReadOnly = true;
            this.textBoxResultElement0.Size = new System.Drawing.Size(52, 23);
            this.textBoxResultElement0.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(30, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 23);
            this.label10.TabIndex = 1;
            this.label10.Text = "1";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 818);
            this.Controls.Add(this.groupBoxResultArray);
            this.Controls.Add(this.buttonSortArray);
            this.Controls.Add(this.buttonReverseArray);
            this.Controls.Add(this.textBoxMultiplication);
            this.Controls.Add(this.labelMultiplication);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.buttonCalculateStatistics);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labelStats);
            this.Controls.Add(this.buttonFillArray);
            this.Controls.Add(this.groupBoxRange);
            this.Controls.Add(this.groupBoxSourceArrays);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBoxSourceArrays.ResumeLayout(false);
            this.groupBoxSourceArrays.PerformLayout();
            this.groupBoxRange.ResumeLayout(false);
            this.groupBoxRange.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBoxResultArray.ResumeLayout(false);
            this.groupBoxResultArray.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxSourceArrays;
        private System.Windows.Forms.Label labelElement0;
        private System.Windows.Forms.TextBox element0;
        private System.Windows.Forms.Label labelElement1;
        private System.Windows.Forms.TextBox element1;
        private System.Windows.Forms.Label labelElement2;
        private System.Windows.Forms.TextBox element2;
        private System.Windows.Forms.Label labelElement3;
        private System.Windows.Forms.TextBox element3;
        private System.Windows.Forms.Label labelElement4;
        private System.Windows.Forms.TextBox element4;
        private System.Windows.Forms.Label labelElement5;
        private System.Windows.Forms.TextBox element5;
        private System.Windows.Forms.Label labelElement6;
        private System.Windows.Forms.TextBox element6;
        private System.Windows.Forms.Label labelElement7;
        private System.Windows.Forms.TextBox element7;
        private System.Windows.Forms.Label labelElement8;
        private System.Windows.Forms.Label labelElement9;
        private System.Windows.Forms.TextBox element8;
        private System.Windows.Forms.TextBox element9;
        private System.Windows.Forms.Label labelRangeFrom;
        private System.Windows.Forms.Label labelabelRangeTo;
        private System.Windows.Forms.TextBox rangeFrom;
        private System.Windows.Forms.TextBox rangeTo;
        private System.Windows.Forms.GroupBox groupBoxRange;
        private System.Windows.Forms.Button buttonFillArray;
        private System.Windows.Forms.Label labelStats;
        private System.Windows.Forms.Label labelSumElements;
        private System.Windows.Forms.TextBox textBoxSumElements;
        private System.Windows.Forms.Label labelAverage;
        private System.Windows.Forms.TextBox textBoxAverage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonCalculateStatistics;
        private System.Windows.Forms.Label labelMinimum;
        private System.Windows.Forms.Label labelMinimumElementIndex;
        private System.Windows.Forms.TextBox minimumElementIndex;
        private System.Windows.Forms.Label labelMinimumElementValue;
        private System.Windows.Forms.TextBox minimumElementValue;
        private System.Windows.Forms.Label labelMaximum;
        private System.Windows.Forms.Label labelMaximumElementIndex;
        private System.Windows.Forms.TextBox maximumElementIndex;
        private System.Windows.Forms.Label labelMaximumElementValue;
        private System.Windows.Forms.TextBox maximumElementValue;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelSumPositive;
        private System.Windows.Forms.TextBox textBoxSumPositive;
        private System.Windows.Forms.Label labelSumNegative;
        private System.Windows.Forms.TextBox textBoxSumNegative;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label labelCountPositiveEven;
        private System.Windows.Forms.TextBox textBoxCountPositiveEven;
        private System.Windows.Forms.Label labelCountPositiveOdd;
        private System.Windows.Forms.TextBox textBoxCountPositiveOdd;
        private System.Windows.Forms.Label labelCountNegativeEven;
        private System.Windows.Forms.TextBox textBoxCountNegativeEven;
        private System.Windows.Forms.Label labelCountNegativeOdd;
        private System.Windows.Forms.TextBox textBoxCountNegativeOdd;
        private System.Windows.Forms.TextBox textBoxMultiplication;
        private System.Windows.Forms.Label labelMultiplication;
        private System.Windows.Forms.Button buttonReverseArray;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxResultElement0;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxResultElement1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxResultArray;
        private System.Windows.Forms.Button buttonSortArray;
        private System.Windows.Forms.TextBox textBoxResultElement2;
        private System.Windows.Forms.TextBox textBoxResultElement3;
        private System.Windows.Forms.TextBox textBoxResultElement4;
        private System.Windows.Forms.TextBox textBoxResultElement5;
        private System.Windows.Forms.TextBox textBoxResultElement6;
        private System.Windows.Forms.TextBox textBoxResultElement7;
        private System.Windows.Forms.TextBox textBoxResultElement8;
        private System.Windows.Forms.TextBox textBoxResultElement9;
    }
}
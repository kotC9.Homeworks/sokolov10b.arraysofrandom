﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * Практикум. “Массивы случайных чисел”
 * 10Б
 * Соколов Сергей
 *
 * P.S. Ненавижу паскаль
 */

namespace sokolov10b.arraysofrandom
{
    public partial class Form1 : Form
    {
        private List<TextBox> _numberTextBoxList;
        private List<TextBox> _resultNumberTextBoxList;
        private List<int> _numbers = new List<int>();

        public Form1()
        {
            InitializeComponent();
            _numberTextBoxList = new List<TextBox>
            {
                element0, element1, element2, element3, element4,
                element5, element6, element7, element8, element9
            };
            
            _resultNumberTextBoxList = new List<TextBox>
            {
                textBoxResultElement0, textBoxResultElement1, textBoxResultElement2, textBoxResultElement3, textBoxResultElement4, 
                textBoxResultElement5, textBoxResultElement6, textBoxResultElement7, textBoxResultElement8, textBoxResultElement9, 
            };
        }

        private void buttonFillArray_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(rangeFrom.Text) || string.IsNullOrEmpty(rangeTo.Text))
            {
                MessageBox.Show("Введите границы диапазона");
                return;
            }

            if (!int.TryParse(rangeFrom.Text, out var from) || !int.TryParse(rangeTo.Text, out var to))
            {
                MessageBox.Show("Введите число");
                return;
            }

            // заполнение полей числами
            var random = new Random();

            foreach (var element in _numberTextBoxList)
            {
                element.Text = random.Next(from, to + 1).ToString();
            }
            
            // создание массива с числами
            foreach (var box in _numberTextBoxList)
            {
                _numbers.Add(int.Parse(box.Text));
            }
        }


        private void buttonCalculateStatistics_Click(object sender, EventArgs e)
        {
            //выход если массив еще не сгенерирован
            if (_numberTextBoxList.Any(element => string.IsNullOrEmpty(element.Text)))
            {
                return;
            }
            
            
            CalculateFirstGroup();
            CalculateSecondGroup();
            CalculateThirdGroup();


            //произведение элементов
            var multiplication = _numberTextBoxList.Aggregate(1, (current, box) => current * int.Parse(box.Text));
            textBoxMultiplication.Text = multiplication.ToString();
        }

        private void buttonReverseArray_Click(object sender, EventArgs e)
        {
            //выход если массив еще не сгенерирован
            if (_numberTextBoxList.Any(element => string.IsNullOrEmpty(element.Text)))
            {
                return;
            }
            
            var count = _numberTextBoxList.Count;
            for(var i = 0; i < count;i++)
            {
                _resultNumberTextBoxList[i].Text = _numberTextBoxList[count - 1 - i].Text;
            }
        }

        private void buttonSortArray_Click(object sender, EventArgs e)
        {
            //выход если массив еще не сгенерирован
            if (_numberTextBoxList.Any(element => string.IsNullOrEmpty(element.Text)))
            {
                return;
            }
            
            var count = _numbers.Count;
            var sorted = _numbers.OrderBy(element => element).ToList();

            for(var i = 0; i < count;i++)
            {
                _resultNumberTextBoxList[i].Text = sorted[i].ToString();
            }
        }
        
        
        private void CalculateFirstGroup()
        {
            var minimum = _numbers[0];
            var minimumIndex = 0;
            
            var maximum = _numbers[0];
            var maximumIndex = 0;
            for(var i = 1; i< _numbers.Count;i++)
            {
                if (_numbers[i] < minimum)
                {
                    minimum = _numbers[i];
                    minimumIndex = i;
                }
                
                if (_numbers[i] > maximum)
                {
                    maximum = _numbers[i];
                    maximumIndex = i;
                }
            }
            
            minimumElementIndex.Text = (minimumIndex + 1).ToString();
            minimumElementValue.Text = minimum.ToString();
            
            maximumElementIndex.Text = (maximumIndex + 1).ToString();
            maximumElementValue.Text = maximum.ToString();
        }

        private void CalculateSecondGroup()
        {
            textBoxSumElements.Text = _numbers.Sum().ToString();
            textBoxAverage.Text = _numbers.Average().ToString();
            textBoxSumPositive.Text = _numbers.Where(e => e > 0).Sum().ToString();
            textBoxSumNegative.Text = _numbers.Where(e => e < 0).Sum().ToString();
        }

        private void CalculateThirdGroup()
        {
            textBoxCountPositiveEven.Text = _numbers.Where(e => e > 0 && e % 2 == 0).Sum().ToString();
            textBoxCountPositiveOdd.Text = _numbers.Where(e => e > 0 && e % 2 == 1).Sum().ToString();
            textBoxCountNegativeEven.Text = _numbers.Where(e => e < 0 && e % 2 == 0).Sum().ToString();
            textBoxCountNegativeOdd.Text = _numbers.Where(e => e < 0 && e % 2 == 1).Sum().ToString();
        }
    }
}